//
//  FlickrAPI.h
//  FlickrBreaktoutDemo
//
//  Created by James Cash on 22-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrPhoto.h"

@interface FlickrAPI : NSObject

+ (void)searchFor:(NSString*)query complete:(void(^)(NSArray* searchResults))complete;

+ (void)loadImage:(FlickrPhoto*)photo complete:(void (^)(UIImage* image))complete;

@end
