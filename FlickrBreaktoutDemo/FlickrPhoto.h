//
//  FlickrPhoto.h
//  FlickrBreaktoutDemo
//
//  Created by James Cash on 22-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrPhoto : NSObject

@property (nonatomic,strong) NSString* server;
@property (nonatomic,strong) NSString* farm;
@property (nonatomic,strong) NSString* photoId;
@property (nonatomic,strong) NSString* secret;

- (instancetype)initWithInfo:(NSDictionary*)info;

- (NSURL*)url;

@end
