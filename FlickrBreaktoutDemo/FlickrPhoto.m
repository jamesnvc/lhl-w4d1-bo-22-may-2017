//
//  FlickrPhoto.m
//  FlickrBreaktoutDemo
//
//  Created by James Cash on 22-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "FlickrPhoto.h"

@implementation FlickrPhoto

- (instancetype)initWithInfo:(NSDictionary*)info
{
    if ([super init]) {
        _server = info[@"server"];
        _farm = info[@"farm"];
        _secret = info[@"secret"];
        _photoId = info[@"id"];
    }
    return self;
}

- (NSURL *)url
{
    return [NSURL URLWithString:
            [NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@.jpg",
             self.farm, self.server, self.photoId, self.secret]];
}

@end
