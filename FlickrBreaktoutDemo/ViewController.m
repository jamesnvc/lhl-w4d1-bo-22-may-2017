//
//  ViewController.m
//  FlickrBreaktoutDemo
//
//  Created by James Cash on 22-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "FlickrAPI.h"

@interface ViewController ()

@property (nonatomic,strong) NSArray* photoResults;
@property (nonatomic,strong) UIImageView* someImageViewWeHave;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [FlickrAPI searchFor:@"dog"
                complete:^(NSArray *searchResults) {
                    NSLog(@"found %@", searchResults);
                    self.photoResults = searchResults;
                    [self displayFirstImage];
                }];


    // what we want is something like
    // self.imgView.image = [[[FlickAPI searchFor:@"dog] firstObject] loadImage]
}

- (void)displayFirstImage
{
    // what we want
    // self.someImageViewWehave.image = [FlickAPI loadImage:self.photoResults[0]];
    //
    [FlickrAPI loadImage:self.photoResults[0]
                complete:^(UIImage *image) {
                    // [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        self.someImageViewWeHave.image = image;
                    //}];
                }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
